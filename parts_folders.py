# Vernon Weaver
# 070721

# Takes a parts list (in Visual import format)
# | ITEM | QTY_PER | PART_ID | DESCRIPTION | JUNK-TEXT | ....

# and generates folders in the SkyCAD Parts format 
# Appends an X to the end of the folder to indicate that 
# this is an empty folder. The X should be deleted when data
# is added to the folder


import argparse
import xlrd
import os
import re

parser = argparse.ArgumentParser()
parser.add_argument("folder", type=str, help="the root folder in which to create the part subfolders")
parser.add_argument("file", type=str, help="the excel file containing the parts list")
args = parser.parse_args()

if not os.path.isdir(args.folder):
    print("Invalid folder path. Please ensure that the folder exists and the path is correct")
    exit()

if not os.path.exists(args.file):
    print("Invalid file path. Please ensure that the .xls file exists and the path is correct")
    exit()

filetype = args.file.split(".")[-1]
if filetype == "xls":
    book = xlrd.open_workbook(args.file)
    sh = book.sheet_by_index(0)
    for rx in range(1, sh.nrows):
        pendu_pn = sh.row(rx)[2].value
        description = sh.row(rx)[5].value
        mfg_pn = sh.row(rx)[4].value
        if pendu_pn and description and mfg_pn: # skip incomplete lines
            folder_name = f"{pendu_pn}, {description}, {mfg_pn} X"
            folder_name = re.sub(r'[\\/\:*"<>\|\.%\$\^&£]', '', folder_name) # remove illegal characters
            path = os.path.join(args.folder, folder_name)
            print(path)
            os.makedirs(path, exist_ok=True)
# using this construct to make it easier to add other file types in the future
elif filetype == "xlsx":
    print(".xlsx files are not supported. Save as .xls")
elif filetype == ".csv":
    print(".csv files are not supported. Save as .xls")
else:
    print("Please provide an .xls file") 
